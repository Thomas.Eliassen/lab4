package datastructure;

import java.util.ArrayList;
import java.util.List;

import cellular.CellState;

public class CellGrid implements IGrid {
	private List<List<CellState>> table;
	private int rows;
	private int cols;
	
    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
		this.cols = columns;
		this.table = new ArrayList<List<CellState>>(0);
		
		for (int i = 0; i <= rows; i++)  {
			table.add(new ArrayList<>(0));
			for (int j = 0; j <= cols; j++) {
				table.get(i).add(initialState);
		    }
		}
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	
    	if (row > numRows() || column > numColumns()) {
    		throw new IndexOutOfBoundsException();
    	}
    	if (row < 0 || column < 0) {
        	throw new IndexOutOfBoundsException();
    	}
    	table.get(row).set(column,element);
    }

    @Override
    public CellState get(int row, int column) {
    	if (row > numRows() || column > numColumns()) {
    		throw new IndexOutOfBoundsException();
    	}
    	if (row < 0 || column < 0) {
        	throw new IndexOutOfBoundsException();
    	}
        return table.get(row).get(column);
    }

    @Override
    public IGrid copy() {
    	CellGrid copyTable = new CellGrid(rows,cols,CellState.DEAD);
    	CellState CurrentCellState;
    	for (int i=0; i<=rows; i++) {
    		for (int j=0; j<=cols; j++) {
    			CurrentCellState = get(i, j);
    			copyTable.set(i,j,CurrentCellState);
    			}
    		}
        return copyTable;
    }
}